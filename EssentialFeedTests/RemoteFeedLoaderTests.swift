//
//  RemoteFeedLoaderTests.swift
//  EssentialFeedTests
//
//  Created by Ashish Stephen on 2/15/24.
//

import XCTest

class RemoteFeedLoaderTests: XCTestCase {
    
    class RemoteFeedLoader {
        
    }
    
    class HTTPClient {
        var requestedURL: URL?
    }
    
    func test_init_doesNotRequestDataFromURL() {
        let sut = RemoteFeedLoader()
        let client = HTTPClient()
        
        XCTAssertNil(client.requestedURL)
    }
    
    
}
