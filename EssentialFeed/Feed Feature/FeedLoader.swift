//
//  FeedLoader.swift
//  EssentialFeed
//
//  Created by Ashish Stephen on 2/15/24.
//

import Foundation

enum LoadFeedResult {
    case success([FeedItem])
    case error(Error)
}

protocol FeedLoader {
    func load(completion: @escaping (LoadFeedResult)->Void)
}
