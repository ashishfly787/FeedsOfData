//
//  FeedItem.swift
//  EssentialFeed
//
//  Created by Ashish Stephen on 2/15/24.
//

import Foundation

struct FeedItem {
    let id: UUID
    let description: String?
    let location: String?
    let imageUrl: URL
}
